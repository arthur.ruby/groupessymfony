<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 * @package App\Controller
 * @Route("/", name="main_")
 */
class MainController extends AbstractController
{
	/** controleur en charge de rediriger vers vue principale accueil
	 * @Route("/", name="home")
	 * @return Response redirection vers vue principale accueil
	 */
    public function home()
    {
        return $this->render('main/home.html.twig',
			[
            	"title" => "Accueil",
        	]);
    }

	/** controleur en charge de rediriger vers vue à propos twig
	 * @return Response redirection vers vue à propos twig
	 * @Route(path="/about", name="about")
	 */
    public function about()
	{
		return $this->render("main/about.html.twig",
			[
				"title" => "A propos"
			]);
	}
}
