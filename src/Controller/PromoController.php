<?php

namespace App\Controller;

use App\Entity\Promo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PromoController extends AbstractController
{
    /** Méthode en charge de retourner la vue de groupe
     * @Route("/promo", name="promo")
     */
    public function promoIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $promo = new Promo();
        $promo->setNom("D2WM-06");
        $em->persist($promo);
        $em->flush();
    }
}
