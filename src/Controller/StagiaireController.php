<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StagiaireController qui gère l'accès aux objets stagiaire et leur affichage
 * @package App\Controller
 * @Route(path="/stagiaire", name="stagiaire_")
 */
class StagiaireController extends AbstractController
{
	private $liste =
		[
			"stagiaire1" =>
			[
				"id" => "1",
				"prenom" => "Arthur",
				"nom" => "Smart",
				"ville" => "San Francisco"
			],
			"stagiaire2" =>
			[
				"id" => "2",
				"prenom" => "Jean-Michel",
				"nom" => "Doudoux",
				"ville" => "Luxembourg Ville"
			],
			"stagiaire3" =>
				[
					"id" => "3",
					"prenom" => "Wololo",
					"nom" => "Zololo",
					"ville" => "Xololo"
				],
			];

    /** méthode en charge de rediriger vers la vue de liste des stagiaires
     * @Route("/", name="liste")
     * @return Response
     */
    public function liste()
    {
    	return $this->render("stagiaire/liste.html.twig",
		    [
            "title" => "Liste des Stagiaires",
		    "liste" => $this->liste
            ]);
    }

	/** méthode en charge de rediriger vers la vue de détail d'un stagiaire
	 * @Route("/detail/{id}", requirements={"id"="\d+"}, name="detail")
	 * @param $id int identifiant du stagiaire
	 * @return Response
	 */
    public function detail($id)
    {
    	return $this->render("stagiaire/detail.html.twig",
		    [
		        "title" => "Détail du stagiaire",
			    "stagiaire" => $this->liste["stagiaire" . $id]
		    ]);
    }
}
